import copy
import random
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.patches import Patch

from Job import Job
from Operation import Operation

L = 3 #number of machines and operations
NUM_OPERATIONS = 3
POPULATION_SIZE = 30
PARENT_1_PROBABILITY_TRESHOLD = 0.45
PARENT_2_PROBABILITY_TRESHOLD = 0.9 # 0.45 - 0.9
STANDBY = "standby"
GENERATIONS = 1000

'''
GENES_O1 = []
GENES_O2 = []
GENES_O3 = []
job_1 = Job([Operation(1, 8, 'J1O1'), Operation(3, 7, 'J1O2'), Operation(2, 5, 'J1O3')])
job_2 = Job([Operation(2, 20, 'J2O1'), Operation(1, 9, 'J2O2'), Operation(3, 2, 'J2O3')])
job_3 = Job([Operation(3, 1, 'J3O1'), Operation(2, 3, 'J3O2'), Operation(1, 1, 'J3O3')])
jobs = [job_1, job_2, job_3]
for i in range(len(jobs)):
    GENES_O1.append(jobs[i].operations[0])
    GENES_O2.append(jobs[i].operations[1])
    GENES_O3.append(jobs[i].operations[2])
'''
job_1 = Job([Operation(1, 8, 'J1O1'), Operation(3, 7, 'J1O2'), Operation(2, 5, 'J1O3')])
job_2 = Job([Operation(1, 20, 'J2O1'), Operation(2, 9, 'J2O2'), Operation(3, 2, 'J2O3')])
job_3 = Job([Operation(3, 1, 'J3O1'), Operation(2, 3, 'J3O2'), Operation(1, 1, 'J3O3')])
job_4 = Job([Operation(2, 10, 'J4O1'), Operation(1, 5, 'J4O2'), Operation(3, 8, 'J4O3')])
job_5 = Job([Operation(1, 15, 'J5O1'), Operation(2, 6, 'J5O2'), Operation(3, 4, 'J5O3')])
job_6 = Job([Operation(3, 3, 'J6O1'), Operation(1, 7, 'J6O2'), Operation(2, 6, 'J6O3')])
jobs = [job_1, job_2, job_3,job_4,job_5,job_6]

GENES = [[] for _ in range(NUM_OPERATIONS)] #INITIALIZING GENES!!
for j in range(NUM_OPERATIONS): #GENES IS GOING TO BE L-long (L => number of operations) and n-th list will have n-th operation of every job
    for i in range(len(jobs)):
        GENES[j].append(jobs[i].operations[j])
        continue

class Individual(): #class individual that posseses chromosome and fitness attributes and all of functions for genetic algorithm
    def __init__(self):
        self.chromosome = [[] for _ in range(L)]
        self.fitness = 0

    def print_individual(self):
        for i in range(len(self.chromosome)):
            machine = []
            for j in range(len(self.chromosome[i])):
                machine.append(f'{self.chromosome[i][j].name} : {self.chromosome[i][j].t}')
            print(machine)
    '''
    @ Function for creating random solution. It serves as tool for putting operations at right machines in radnom order.
    @ More jobs we are given to do,more variations of solution we can have.
    '''
    def create_gnome(self):
        global GENES

        copy_genes = copy.deepcopy(GENES) #deep copy so the genes wouldnt change
        for i in range(len(copy_genes)):
            random.shuffle(copy_genes[i]) #we shuffle the list so the pick would be random
            for _ in range(len(jobs)):
                idx = random.randint(0,199692)%len(copy_genes[i])
                elem = copy_genes[i].pop(idx) #poping random index and putting it to right machine
                #print(elem.name)
                machine_idx = elem.machine - 1
                self.chromosome[machine_idx].append(elem)


    '''
    @Most complicated function in algorithm path. It uses continuous for loops for checking operations of same jobs.
    @It mainly serves to put standby time between n-th and n+1-th operation of same job.
    @ 1)For every operation on machine, we search for n+1 operation of same job on other machines.
    @ 2)If current element we check is next operation of the same job, we check for comparations of sums
    @ 3) Comparation of sum of time of all operations before found n+1-th operation 
        and sum of time of all operations before n-th operation(including n-th operation).
    @ 4) If sum1<sum2, we must create Operation object that is going to represent standby time on that machine,
         and add time difference to standby (sum2 - sum1) that is going to represent idle time of that machine.
         That standby Operation is going to be placed right before n+1-th operation.
         Else, we do nothing and move on with loop
    '''
    def fit_solution(self):
        '''
        [
        [J1O1, J2O2, J3O3]    [J1O1, 12E, J2O2, J3O3]
        [J2O1, J3O2, J1O3] -> [J2O1, J3O2, J1O3]
        [J3O1, J1O2, J2O3]    [J3O1, 7E, J1O2, 14E J2O3]
        ]
        '''
        L = len(self.chromosome)

        for i in range(L): #machines
            for j in range(len(self.chromosome[i])): #operations on curr machine
                if self.chromosome[i][j].name == "standby":
                    continue
                for k in range(L): #for machines
                    if k != i :
                        for l in range(len(self.chromosome[k])): #for operations in k-th machine
                            if self.chromosome[k][l].name == STANDBY or int(self.chromosome[k][l].name[3]) == 1 : #if it is already standby elem we skip
                                continue

                            if self.chromosome[k][l].name[:2] == self.chromosome[i][j].name[:2] and int(self.chromosome[k][l].name[3]) == int(self.chromosome[i][j].name[3])+1:
                                #if job string (example => J1 == J1) match up and found operation is the next operation of our job
                                check_sum = sum(self.chromosome[i][m].t for m in range(j+1))
                                test_sum = sum(self.chromosome[k][n].t for n in range(l)) #check sum and test sum
                                if test_sum<check_sum: #if one on next found element is smaller than check sum
                                    time_difference = check_sum - test_sum #we count time difference and to that machine,before found operation
                                                                        # we put standby with self.t attribute equal to time_difference
                                    self.chromosome[k].insert(l,Operation(k+1,time_difference,"standby"))

    '''
    @Function for striping standby-s out of parent machines, so we can fit solution for child later
    @This prevents multiple standby-s one after another!
    '''
    def strip_solution(self):
        new_chromosome = [[] for _ in range(len(self.chromosome))]
        # Reverses the effect of the function above, removes standbys
        for i in range(len(self.chromosome)):
            for j in range(len(self.chromosome[i])):
                if self.chromosome[i][j].name != 'standby':
                    new_chromosome[i].append(self.chromosome[i][j])
        return new_chromosome

    '''
    @ Fitnes in case of jobshop problem is MAKESPAN of solution, or maximum time elapsed on three machine.
    @ If for example => M1 = 32s , M2 = 22s , M3 = 40s => MAKESPAN IS 40s,because thats where whole proccess and all operations end
    '''
    def calculate_fitness(self):
        '''
        makespan -> max(machine.worktime for machine in self.chromosome)
        '''
        makespan = max(sum(self.chromosome[i][j].t for j in range(len(self.chromosome[i]))) for i in range(len(self.chromosome)))
        self.fitness = makespan

    '''
    @ Function for random generating order on one particular machine.
    @ It is called when neither parent 1 or 2 can set their machine.
    '''

    def mutate(self, machine):
        global GENES
        copy_genes = copy.deepcopy(GENES) #GENES DEEPCOPY
        for i in range(len(copy_genes)):
            valid_genes = [] #VALID GENES (VALID GENE IS IF current operation machine is the one we are looking for
            random.shuffle(copy_genes[i])
            for j in range(len(jobs)): #we go through whole operation list(operation by operation and if machine is the one looked for
                if copy_genes[i][j].machine != machine + 1:
                    continue
                valid_genes.append(copy_genes[i][j])
            for _ in range(len(valid_genes)):
                idx = random.randint(0,199692)%len(valid_genes)
                elem = valid_genes.pop(idx)
                self.chromosome[machine].append(elem)

    '''
    @Function for crossing genes(in this case machine order) of parents
    @We use probability threshold and random probability function
    @If probability is out of range of both thresholds, we must mutate gene (randomly select it)
    '''
def cross(parent1,parent2): #we have probability thresholds and random.random() function creates random probability
    global PARENT_1_PROBABILITY_TRESHOLD
    global PARENT_2_PROBABILITY_TRESHOLD
    child = Individual()
    i = 0
    for gene_1, gene_2 in zip(parent1, parent2): #for machine
        probability = random.randint(0, 100) / 100 #random
        if probability < PARENT_1_PROBABILITY_TRESHOLD:
            child.chromosome[i] = gene_1
        elif probability < PARENT_2_PROBABILITY_TRESHOLD:
            child.chromosome[i] = gene_2
        else:
            child.mutate(i)
        i += 1 #incrementing i for every machine
    return child #return child without standby-s included(later we are going to call fit_solution())_
def visualize_schedule(jobs, schedule):
    global STANDBY
    plt.figure()
    num_machines = max(operation.machine for job in jobs for operation in job.operations) + 1

    # Plot the schedule
    colors = sns.color_palette("husl", 6)
    job_entries = []
    for i in range(len(colors)):
        job_entries.append(Patch(color = colors[i],label = f'Job{i+1}'))
    plt.legend(handles = job_entries, loc = 'upper right')
    for i in range(len(schedule.chromosome)):
        for j in range(len(schedule.chromosome[i])):
            operation = schedule.chromosome[i][j]
            if operation.name == STANDBY:
                continue
            start_time = sum(schedule.chromosome[i][k].t for k in range(j))
            end_time = sum(schedule.chromosome[i][k].t for k in range(j+1))
            plt.barh(L-i, schedule.chromosome[i][j].t, left=sum(schedule.chromosome[i][k].t for k in range(j)),label=f'Job{operation.name[1]}', color=colors[int(operation.name[1])-1])
            text_x = 0.5*(start_time+end_time)
            text_y = L-i
            plt.text(text_x, text_y, f'{operation.name[2:]}\n {operation.t}s', color='black', ha='center', va='center', fontsize=8)
    plt.yticks(range(num_machines), [f'Machine {i}' for i in range(num_machines)])
    plt.xlabel('Time')
    plt.title('Job Shop Scheduling with Genetic Algorithm')
    plt.show()
def graphic_of_evaluation(generations):
    generations = np.array(generations)
    x = generations[:,0]
    y = generations[:,1]
    plt.figure()
    plt.title("Evaluation of algorithm")
    plt.plot(x,y,color = "blue")
def check_solution(population):
    optimum = population[0]
    global STANDBY
    for machine in (optimum.chromosome):
        for k in range (len(machine)):
            if machine[k].name == STANDBY:
                return optimum
    return population[1]
#DRIVER CODE
if __name__ == "__main__":
    generations = [] #for graphic
    current_gen_fitnesses = []

    #current generation 
    generation = 1
  
    #found = False
    population = []
  
    # create initial population 
    for _ in range(POPULATION_SIZE):
                gnome = Individual()
                gnome.create_gnome()
                gnome.fit_solution()
                gnome.calculate_fitness()
                population.append(gnome)

  
    for _ in range(GENERATIONS): 
  
        # sort the population in increasing order of fitness score 
        population = sorted(population, key = lambda x:x.fitness)
  
        # Otherwise generate new offsprings for new generation 
        new_generation = []
  
        #elitism
        s = int(POPULATION_SIZE*0.1)
        new_generation.extend(population[:s])
  
        # mating 50% of the fittest
        s = int(POPULATION_SIZE*0.9)
        for _ in range(s):
            parent1 = random.choice(population[:POPULATION_SIZE//2]) 
            parent2 = random.choice(population[:POPULATION_SIZE//2])
            genes1 = parent1.strip_solution()
            genes2 = parent1.strip_solution()
            child = cross(genes1,genes2)
            child.fit_solution()
            child.calculate_fitness()
            new_generation.append(child)
            current_gen_fitnesses.append(child.fitness)
            #adding to graphic list
        generations.append([generation,sum(current_gen_fitnesses)//(len(current_gen_fitnesses))]) #we calculate average fitness of a child in generations
        #we then append number of generation and average fitness, so we can later put it onto plot!
        current_gen_fitnesses = []
        population = new_generation
        generation += 1

    optimum = population[0]
    optimum.print_individual()
    print()
    print(f'fitness: {optimum.fitness}')

    graphic_of_evaluation(generations)
    visualize_schedule(jobs, optimum)